# Scripts, Configs, stuff I use

This repo holds some of the stuff from my `/home/gunix/`

Good stuff you can find here:

* i3wm config
* .zshrc 
* some kvm scripts

This repo also used to have some scripts to install Openstack & Openshift but they got deprecated so I removed them.