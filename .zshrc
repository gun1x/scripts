# This was build by "Mr.Elendig" Heggstad <mrelendig@har-ikkje.net> ... he is awesome.
# Original is linked on the archlinux wiki
# However, I edited parts of it so that it fits my requirements.

#-----------------------------
# Source some stuff
#-----------------------------
if [[ -f /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]]; then
  . /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

BASE16_SHELL="$HOME/.config/base16-shell/base16-default.dark.sh"
[[ -s $BASE16_SHELL ]] && source $BASE16_SHELL

#------------------------------
# History stuff
#------------------------------
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

#------------------------------
# Variables
#------------------------------
export EDITOR="vim"
export GOPATH=$HOME/.go
export CGO_ENABLED=0

#------------------------------
# Keybindings
#------------------------------
bindkey -v
typeset -g -A key

bindkey -a '^?' backward-delete-char
bindkey -a '^[[5~' up-line-or-history
bindkey -a '^[[3~' delete-char
bindkey -a '^[[6~' down-line-or-history
bindkey -a '^[[A' up-line-or-search
bindkey -a '^[[D' backward-char
bindkey -a '^[[B' down-line-or-search
bindkey -a '^[[C' forward-char
bindkey -a "^[[H" beginning-of-line
bindkey -a "^[[F" end-of-line

bindkey '^R' history-incremental-search-backward
bindkey '^?' backward-delete-char
bindkey '^[[5~' up-line-or-history
bindkey '^[[3~' delete-char
bindkey '^[[6~' down-line-or-history
bindkey '^[[A' up-line-or-search
bindkey '^[[D' backward-char
bindkey '^[[B' down-line-or-search
bindkey '^[[C' forward-char
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line

bindkey '\eOH'  beginning-of-line
bindkey '\eOF'  end-of-line

#------------------------------
# Alias stuff
#------------------------------
alias ls="ls --color -F"
alias ll="ls --color -lh"
alias gr="gvim --remote-silent"
alias vr="vim --remote-silent"


#------------------------------
# colored man
#------------------------------
man() {
    LESS_TERMCAP_md=$'\e[01;33m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;34m' \
    command man "$@"
}



#------------------------------
# Comp stuff
#------------------------------
zmodload zsh/complist 
autoload -Uz compinit
compinit
zstyle :compinstall filename '${HOME}/.zshrc'

#- buggy
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'
#-/buggy


zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}


# vim: set ts=2 sw=2 et:
#

push-all-now() {
    git add .
    git commit -m "$*"
    git push
}
alias push=push-all-now

alias ls='ls --color=auto'
alias grep='grep --colour=auto'
setopt noautomenu


# so that you can select quoted
autoload -U select-quoted
zle -N select-quoted
for m in visual viopp; do
  for c in {a,i}{\',\",\`}; do
    bindkey -M $m $c select-quoted
  done
done

#allow interactive comments
setopt interactivecomments

#powerline-daemon -q
#. /usr/lib/python3.8/site-packages/powerline/bindings/zsh/powerline.zsh

source /home/gunix/.hidden_aliases.sh

#/home/gunix/git/gun1x/razer_keyboard_config/advanced_effect.py
#alias vim="/home/gunix/git/gun1x/razer_keyboard_config/arch_keyboard.py; vim"

nom_to_png() {
    cat $1 | docker run --rm -i dapariscode/nomnoml-cli /bin/bash -c "cat | ./nomnoml" > $1.png.tmp
    mv $1.png.tmp $1.png
}

function mdv() {
  docker run --rm -v $PWD:/sandbox -w /sandbox -it rawkode/mdv:latest $@
}

if [[ $TERM == xterm-termite ]]; then
  . /etc/profile.d/vte.sh
fi

alias ssh='TERM=xterm ssh'

alias status='~/.status.sh'
status

alias highlight='highlight --config-file=$(find /usr/share/highlight/themes/ -type f | shuf -n 1)'

function powerline_precmd() {
    PS1="$($GOPATH/bin/powerline-go -error $? \
      -newline -static-prompt-indicator \
      -modules cwd,perms,git,hg,jobs,exit,root \
      -shell zsh)"
}

function install_powerline_precmd() {
  for s in "${precmd_functions[@]}"; do
    if [ "$s" = "powerline_precmd" ]; then
      return
    fi
  done
  precmd_functions+=(powerline_precmd)
}

if [ "$TERM" != "linux" ]; then
    install_powerline_precmd
fi
