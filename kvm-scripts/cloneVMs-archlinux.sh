# This script can be used to clone VMs fast
# It will automatically assign IP and hostname to the VM
# Make sure you have ssh keys within the template, for your user and for root

# Feel free to commit!


if [ $# -ne 1 ]
  then
    echo "Usage: ./cloneTheVM-archlinux.sh new-vm-name"
    echo "IP is not required for this script. You have to configure your dnsmasq server to allow DHCP Hostname resolution."
    echo " The script will pick by default the vm named archlinux as template"
    exit 1
fi

templateName="archlinux"
requiredVMName=$1

echo creating $requiredVMName from $templateName

sudo virt-clone --original $templateName --name $requiredVMName --file /opt/raid/vms/$requiredVMName.qcow2
sudo chown root:kvm /opt/raid/vms/$requiredVMName.qcow2

sudo virsh start $requiredVMName

echo "Waiting for instance to boot ..."

newVMIP="placeholder"

while [[ $newVMIP != 192.168.122.* ]]
do
        newVMIP=$(cat /proc/net/arp | grep $(sudo virsh dumpxml $requiredVMName | grep -oE "([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})") | cut -d" " -f1)
        sleep 5;
done


echo "
127.0.0.1	localhost
127.0.1.1	$requiredVMName $requiredVMName.gunix.test

" | ssh -o StrictHostKeyChecking=no root@$newVMIP "cat > /etc/hosts"

cat /etc/hosts | grep -v 127.0 | ssh root@$newVMIP "cat >> /etc/hosts"

echo "$requiredVMName.gunix.test" | ssh root@$newVMIP "cat > /etc/hostname"

ssh root@$newVMIP systemctl disable NetworkManager
ssh root@$newVMIP systemctl enable dhcpcd


ssh root@$newVMIP reboot
echo done
