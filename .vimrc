source $VIMRUNTIME/defaults.vim
set mouse=""
set nu
set laststatus=2
set t_Co=256
set wildmode=longest,list
let g:powerline_pycmd = 'py3'
nnoremap <C-Up> <C-W><C-K>
nnoremap <C-Down> <C-W><C-J>
nnoremap <C-Right> <C-W><C-L>
nnoremap <C-Left> <C-W><C-H>
nnoremap <F2> :set invnu <CR>
nnoremap <F3> :set mouse=a <CR>
nnoremap <F4> :set mouse="" <CR>
nnoremap <F5> :terminal % <CR>
nnoremap <F6> :w <CR> :GoTestCompile <CR> <CR>
nnoremap <F7> :GoCoverageToggle <CR>
nnoremap <F8> :GoAlternate <CR>
inoremap <F6> <ESC> :w <CR> :GoTestCompile <CR> <CR>

colorscheme pablo
hi Folded ctermbg=232 ctermfg=33
set splitbelow
set splitright
set shiftwidth=2
set expandtab
set softtabstop=2
set foldmethod=syntax
set foldnestmax=1

autocmd BufWritePost * GitGutter
autocmd BufNewFile,BufRead *.fish set syntax=bash
autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4
autocmd FileType go setlocal noexpandtab tabstop=4 shiftwidth=4

let g:indentLine_color_term = 239
let g:ansible_unindent_after_newline = 0

let g:go_auto_sameids = 1
let g:go_highlight_array_whitespace_error = 1
let g:go_highlight_chan_whitespace_error = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_space_tab_error = 1
let g:go_highlight_trailing_whitespace_error = 0
let g:go_highlight_operators = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_parameters = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_generate_tags = 1
let g:go_highlight_string_spellcheck = 1
let g:go_highlight_format_strings = 1
let g:go_highlight_variable_declarations = 1
let g:go_highlight_variable_assignments = 1
let g:go_highlight_diagnostic_errors = 0
let g:go_highlight_diagnostic_warnings = 1
let g:go_fmt_experimental = 1

highlight SignColumn ctermbg=none
highlight GitGutterAdd    ctermfg=12
highlight GitGutterChange ctermfg=13
highlight GitGutterDelete ctermfg=1
highlight goCoverageCovered ctermfg=12
highlight goCoverageUncover ctermfg=1
let g:gitgutter_set_sign_backgrounds = 1
